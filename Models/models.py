import tensorflow as tf
from Models.basics import Encoder, CharEncoder, Decoder, cell, SimpleNlpRnn


class Config():
    def __init__(self, vocab_size=1880, is_test=False):
        '''
        Language Model Configuration Class
        :param vocab_size:
        :param is_test:

        '''
        self.is_test = is_test
        # General
        self.input_vocab_size = vocab_size
        self.output_vocab_size = vocab_size
        self.batch_size = 16
        self.sentence_max_len = 30
        self.input_emb_size = 50
        self.output_emb_size = self.input_emb_size

        self.use_f_cantica = True

        # Encoder
        self.encoder_rnn_size = 200
        self.encoder_keep_prob = 1 if not is_test else 1
        self.proj_layers = None
        self.wrap_attention = False
        self.attention_size = 5
        self.cell_type = "LSTM"

        # LM
        self.set_special_tokens()
        self.proj_size = 256

        # Training
        self.n_steps = 25000
        self.n_epochs = 35
        self.lr = 0.004

    def set_special_tokens(self):
        vocab_size = self.input_vocab_size
        self.special_tokens = []
        self._PAD = vocab_size - 2
        self.special_tokens.append(('<PAD>', self._PAD))
        self._GO = vocab_size - 4
        self.special_tokens.append(('<GO>', self._GO))
        self._EOS = vocab_size - 1
        self.special_tokens.append(('<EOS>', self._EOS))
        self._EOT = vocab_size - 3  # end of terzina
        self.special_tokens.append(('<EOT>', self._EOT))
        self._SEP = vocab_size - 5
        self.special_tokens.append(('<SEP>', self._SEP))

    def get_special_tokens_ids(self):
        return [t[1] for t in self.special_tokens]

    def set_params(self, dict):

        def _recursive_call(items, attr_id):
            items = list(items)
            attr_name = items[attr_id][0]
            attr_values = items[attr_id][1]
            for attr_value in attr_values:
                setattr(self, attr_name, attr_value)
                if attr_id == (len(items) - 1):  # base case[
                    yield self
                else:
                    for i in _recursive_call(items, attr_id + 1):
                        yield i

        items = dict.items()
        for i in _recursive_call(items, 0):
            yield i

    def set_tied_params(self):
        self.output_emb_size = self.input_emb_size
        self.initial_state = self.use_f_cantica
        # self.proj_size = self.output_emb_size


class LanguageModel(object):
    def __init__(self, x, y, config, reuse=False, global_scope="LanguageModel"):
        with tf.variable_scope(global_scope, reuse=reuse):
            self.x = x
            self.y = tf.reshape(y, [-1])

            with tf.variable_scope("Encoder", reuse=reuse):
                '''Initialize RNN state with projection of cantica's feature'''
                self.f_cantica = tf.placeholder(dtype=tf.float32, shape=[None, 3])  # batch_size x f_cantica_len
                with tf.variable_scope("Cantica", reuse=reuse):
                    _init = tf.layers.dense(self.f_cantica, config.encoder_rnn_size, activation=tf.nn.tanh) if config.use_f_cantica else None
                self.cantica_init_state = _init
                self.encoder = SimpleNlpRnn(self.x, config, initial_state=self.cantica_init_state)

            state = self.encoder.rnn_outputs
            state = tf.reshape(state, [-1, config.encoder_rnn_size])
            with tf.name_scope("Output"):
                proj = tf.layers.dense(state, config.input_emb_size)
                # proj = tf.layers.dense(l_context, config.input_emb_size)
                # self.logits = tf.layers.dense(proj, config.output_vocab_size)
                self.logits = tf.matmul(proj, tf.transpose(self.encoder.embeddings))
                batch_size = tf.shape(self.x)[0]
                sentence_max_len = tf.shape(self.x)[1]
                self.preds = tf.reshape(tf.argmax(self.logits, axis=1), [batch_size, sentence_max_len])
                self.sample = tf.reshape(tf.multinomial(self.logits, num_samples=1), [-1])

            with tf.variable_scope("Cost", reuse=reuse):
                self.tg_mask = tf.cast(tf.not_equal(self.y, tf.ones_like(self.y) * config._PAD), tf.float32)


                ce = tf.nn.sparse_softmax_cross_entropy_with_logits(
                    labels=self.y, logits=self.logits)
                self.loss = tf.reduce_sum(ce * self.tg_mask) / tf.reduce_sum(self.tg_mask)
                self.ppl = tf.exp(self.loss)

                self.unweighted_loss = tf.reduce_sum(ce * self.tg_mask) / tf.reduce_sum(self.tg_mask)
                self.ppl = tf.exp(self.unweighted_loss)

                # # CRF LOSS comment it to remove
                # self.transition_params = tf.get_variable("CrfParams", [config.input_vocab_size, config.input_vocab_size])
                # # self.decoded_preds = tf.contrib.crf.crf_decode(self.logits, self.transition_params, sentence_max_len)
                #
                # self.log_likelihood, _ = tf.contrib.crf.crf_log_likelihood(
                #     tf.reshape(self.logits, [batch_size, sentence_max_len, config.input_vocab_size]),
                #     tf.reshape(self.y, [batch_size, sentence_max_len]),
                #     tf.tile([sentence_max_len],[batch_size]),
                #     transition_params=self.transition_params)
                # self.loss = -tf.reduce_mean(self.log_likelihood)

                if not config.is_test:
                    with tf.name_scope("AdamOptimization"):

                        optimizer = tf.train.AdamOptimizer(config.lr)
                        # optimizer = tf.train.GradientDescentOptimizer(config.learning_rate)
                        self.train_op = optimizer.minimize(self.loss)
                        # self.train_op = optimizer.minimize(self.loss, var_list=[v for v in tf.trainable_variables() if "Encoder" not in v.name])

    def act(self, sess, x):
        return sess.run(self.sample, feed_dict={self.x: x})


class PoemGenerator(object):
    def __init__(self, chars, x, y, config, reuse=False):
        self.chars = chars
        self.x = x
        self.y = tf.reshape(y, [-1])
        with tf.variable_scope("PoemGen", reuse=reuse):
            with tf.variable_scope("CharEncoder", reuse=reuse):
                self.char_encoder = CharEncoder(self.chars, config)
                char_encodings = self.char_encoder.char_encodings
                char_encodings = tf.reshape(char_encodings, [-1, 2*config.morph_rnn_size])
                print(char_encodings)

            with tf.variable_scope("Encoder", reuse=reuse):
                self.encoder = Encoder(self.x, config)
                l_context = tf.reshape(self.encoder.left_context, [-1, config.encoder_rnn_size])

            with tf.variable_scope("Output"):
                word_chars_encodings = tf.concat((l_context, char_encodings), axis=1)
                # word_chars_encodings = l_context
                proj = tf.layers.dense(word_chars_encodings, config.input_emb_size)
                self.logits = tf.matmul(proj, tf.transpose(self.encoder.embeddings))
                print(self.logits)
                batch_size = tf.shape(self.x)[0]
                sentence_max_len = tf.shape(self.x)[1]
                print(batch_size)
                self.preds = tf.reshape(tf.argmax(self.logits, axis=1), [batch_size, sentence_max_len])

            with tf.variable_scope("Cost", reuse=reuse):
                print(self.y)
                print(tf.ones_like(self.y))
                self.tg_mask = tf.cast(tf.not_equal(self.y, tf.ones_like(self.y) * config._PAD), tf.float32)
                ce = tf.nn.sparse_softmax_cross_entropy_with_logits(
                    labels=self.y, logits=self.logits)
                self.loss = tf.reduce_sum(ce * self.tg_mask) / tf.reduce_sum(self.tg_mask)
                if not config.is_test:
                    with tf.name_scope("AdamOptimization"):
                        optimizer = tf.train.AdamOptimizer(config.lr)
                        self.train_op = optimizer.minimize(self.loss)


class Generator(object):
    def __init__(self, z, configs, name="Generator", reuse=False):
        config = configs[name]
        with tf.variable_scope(name, reuse=reuse):
            self.z = z
            self.generator = Decoder(self.z, config)
            self.g = self.generator.decoded_outputs  # fixme maybe
            print(self.g)


class Discriminator(object):
    def __init__(self, x, configs, name='Discriminator', reuse=False):
        config = configs[name]
        with tf.variable_scope(name):
            self.x = x
            with tf.variable_scope("Encoder", reuse=reuse):
                with tf.variable_scope("Encoding", reuse=reuse):
                    with tf.variable_scope("fw"):
                        self.sentences_rnn_cell_fw = cell(config.encoder_rnn_size, "LSTM",
                                                          proj=config.proj_layers,
                                                          dropout=config.encoder_keep_prob)
                    with tf.variable_scope("bw"):
                        self.sentences_rnn_cell_bw = cell(config.encoder_rnn_size, "LSTM",
                                                          proj=config.proj_layers,
                                                          dropout=config.encoder_keep_prob)

                    outputs, (fw_state, bw_state) = tf.nn.bidirectional_dynamic_rnn(cell_fw=self.sentences_rnn_cell_fw,
                                                                                    cell_bw=self.sentences_rnn_cell_bw,
                                                                                    inputs=self.x,
                                                                                    dtype=tf.float32)

                self.encodings = tf.concat((fw_state.h, bw_state.h), axis=1)

            with tf.variable_scope("Classifier", reuse=reuse):
                proj = tf.layers.dense(self.encodings, config.proj_size)
                self.o = tf.layers.dense(proj, config.output_size, activation=tf.nn.sigmoid)


class PoemGAN(object):
    def __init__(self, x_s, y_s, generators, discriminators, configs):
        '''
        Constructor of the class that learns to transfer the style of poets.
        :param x_s: dictionary containing input sequences splitted in trigrams.
        :param y_s: dictionary containing original sequences.
        :param generators: dictionary of callable generator classes.
        :param discriminators: dictionary of callable discriminator classes.
        :param config: instance of class related to all the parameters needed for the models.
        '''
        with tf.variable_scope("Poet2Poet"):
            with tf.variable_scope("Encoder"):
                self.z_s = {}
                reuse = False
                for k,x in x_s.items():
                    self.z_s[k] = Encoder(x, configs["shared"], reuse=reuse)
                    reuse = True

            with tf.variable_scope("Generators"):
                self.generators = generators
                self.g_s = {}
                for k, gen in self.generators.items():
                    self.g_s[k] = gen(self.z_s[k].encodings, configs, name=k)

            with tf.variable_scope("Discriminators"):
                self.discriminators = discriminators
                self.dX = {}
                self.dZ = {}
                for k, discr in self.discriminators.items():
                    self.dX[k] = discr(tf.one_hot(y_s[k], depth=configs[k].output_vocab_size), configs, name=k)
                    self.dZ[k] = discr(self.g_s[k].g, configs, name=k, reuse=True)

            with tf.variable_scope("Optimization"):
                optimizer = tf.train.AdamOptimizer(configs["optim"].lr)
                with tf.variable_scope("GeneratorsLoss"):
                    self.g_loss = 0
                    for k,_ in self.generators.items():
                        self.g_loss -= tf.reduce_mean(tf.log(self.dZ[k].o))
                    self.g_train_op = optimizer.minimize(self.g_loss,
                                                         var_list=[v for v in tf.trainable_variables() if "Generators" in v.name])

                with tf.variable_scope("DiscriminatorsLoss"):
                    N = len(self.discriminators)
                    self.neg_weight = 1./float(N-1) if N > 1 else 1.  # neg examples weighted less if they are more

                    self.d_loss = 0
                    for i,_ in self.discriminators.items():
                        self.d_loss -= tf.reduce_mean(tf.log(self.dX[i].o)) #fixme aggiungere .o in discr
                        for j,_ in self.discriminators.items():
                            if i != j:  # maybe if is removable, if so neg_weight should be divided by N not N-1
                                self.d_loss -= self.neg_weight * tf.reduce_mean(tf.log(tf.ones_like(self.dZ[j].o)-self.dZ[j].o))
                    self.d_train_op = optimizer.minimize(self.d_loss,
                                                         var_list=[v for v in tf.trainable_variables() if "Discriminators" in v.name])
