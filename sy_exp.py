import os

import tensorflow as tf
from shutil import copyfile

from datasets.it_syllable_dataset import DanteSyLMDataset
from configs.sy_lm_configs import setup_config
from models.language_model import LanguageModel, sampling_decorator, top_p_sampling, top_k_sampling
from experiments.syllable_lm_experiment import SyLMExperiment
from utils.utils import save_data, print_and_write
# from utils.utils import save_dictionary_tsv

os.environ['CUDA_VISIBLE_DEVICES'] = '0'  # set gpu device (0 is the first gpu, 1 is the second and so on...)

# All Experiment parameters
FLAGS = tf.app.flags.FLAGS

# Experiment Mode: train | eval
tf.app.flags.DEFINE_string('exp_mode', "train", 'Train or Evaluation')

# Vocabulary and Embeddings
tf.app.flags.DEFINE_integer('vocab_size', 1884, 'size of the vocabulary')
tf.app.flags.DEFINE_integer('emb_size', 300, 'size of word embeddings')

# RNNs
tf.app.flags.DEFINE_string('rnn_cell_type', "LSTM", 'cell type')

# Encoder
tf.app.flags.DEFINE_integer('sentence_max_len', 75, 'sequence max length ')
tf.app.flags.DEFINE_integer('enc_size', 1024, 'size of encoder state (if bidirectional it is doubled) ')
tf.app.flags.DEFINE_float('enc_keep_prob', 0.7, 'encoder dropout probability')
tf.app.flags.DEFINE_integer('proj_size', 300, 'size of projection layer')

# Learning
tf.app.flags.DEFINE_bool('restore_model', False, 'restore weights from a pretrained model')
tf.app.flags.DEFINE_integer('n_epochs', 20, 'max number of epochs')
tf.app.flags.DEFINE_integer('batch_size', 32, 'batch size')
tf.app.flags.DEFINE_float('lr', 0.002, 'learning rate')
tf.app.flags.DEFINE_float('norm_clip', 3.0, 'gradient clip norm')
tf.app.flags.DEFINE_integer('max_no_improve', 20, 'number of validations tests without improvements')
tf.app.flags.DEFINE_bool('lr_scheduler', True, 'decreases by half the lr when the model does not improves and it is restored')
tf.app.flags.DEFINE_bool('batch_scheduler', False, 'decreases by half the lr when the model does not improves and it is restored')

# Sampling
# set params for sampling the LM. Depending on the sampling decorator chosen, set here either top_p, top_k or temperature
tf.app.flags.DEFINE_float('top_p', 0.9, 'nucleus sampling probability mass')
tf.app.flags.DEFINE_float('top_k', 10, 'number of k most probable tokens to sample')
tf.app.flags.DEFINE_float('temperature', 0.7, 'multinomial sampling temperature')


if __name__ == '__main__':
    is_test = False
    base_dir = os.path.join(os.getcwd(), "savings")  # experiments directory
    exp_dir = "dante_sy_lm_model_testing_sampling"
    exp_path = os.path.join(base_dir, exp_dir)

    if not os.path.exists(exp_path):
        os.mkdir(exp_path)  # this is the experiment directory
        # copies files for reproducibility purposes
        copyfile(os.path.join(os.getcwd(), os.path.basename(__file__)), os.path.join(exp_path, os.path.basename(__file__)))

    # Getting Configurations
    config, val_config, gen_config = setup_config(FLAGS)

    # Creating Dataset and Vocabulary
    '''To create a dataset with a given vocabulary set sy_vocab. Replace DanteSyLMDataset with another Dataset class 
    to load different kind of data. 
    '''
    poetry_sy_lm_dataset = DanteSyLMDataset(config, sy_vocab=None)
    data_path = os.path.join(os.getcwd(), "data", "la_divina_commedia.txt")  # dataset location, here just the name of the source file
    poetry_sy_lm_dataset.build(data_path, split_size=0.9)  # actual creation of  vocabulary (if not provided) and dataset
    print("Train size: " + str(len(poetry_sy_lm_dataset.train_y)))
    print("Val size: " + str(len(poetry_sy_lm_dataset.val_y)))
    print("Test size: " + str(len(poetry_sy_lm_dataset.test_y)))

    # Saving Configs
    save_data(config, os.path.join(exp_path, 'configs.pkl'))
    save_data(val_config, os.path.join(exp_path, 'val_configs.pkl'))
    save_data(gen_config, os.path.join(exp_path, 'gen_configs.pkl'))

    # Saving Vocabulary
    '''To reuse it in other experiments just do: Vocabulary.load_vocabulary(os.path.join(exp_path, 'vocabulary.pkl'))'''
    poetry_sy_lm_dataset.vocabulary.save_vocabulary(os.path.join(exp_path, 'vocabulary.pkl'))

    # Building Model
    '''Three different model instances, one for training, one for validation and the last for testing.
    Training model has dropout, validation and test models don't. In general there may be other
    different configurations specified for each model. They all share the same weights (reuse=True allows that).'''
    x_ph = tf.placeholder(dtype=tf.int32, shape=[None, None])  # batch_size x sentence_max_len
    y_ph = tf.placeholder(dtype=tf.int32, shape=[None, None])  # batch_size x sentence_max_len
    model_name = "SyllableLMPoemGenerator"

    LanguageModel = sampling_decorator(LanguageModel, top_p_sampling)
    sy_lm_net = LanguageModel(x_ph, y_ph, config, poetry_sy_lm_dataset.vocabulary,  global_scope=model_name)
    sy_lm_net_val = LanguageModel(x_ph, y_ph, val_config, poetry_sy_lm_dataset.vocabulary, global_scope=model_name, reuse=True)
    sy_lm_net_gen = LanguageModel(x_ph, y_ph, gen_config, poetry_sy_lm_dataset.vocabulary, global_scope=model_name, reuse=True)

    # Experiment Run
    '''SyLMExperiment object handles all the experiment. It requires 3 model instances, the dataset and the configuration object.
    It logs performances, tensorflow summaries, and outputs during the training, moreover when FLAGS.lr_scheduler is set to True, it 
    also handles model checkpointing and early stopping.'''
    lm_exp = SyLMExperiment(sy_lm_net, sy_lm_net_val, sy_lm_net_gen, poetry_sy_lm_dataset, exp_path, config)

    mode = FLAGS.exp_mode
    if mode == "train":
        print("\nTraining\n")
        lm_exp.run()
        lm_exp.config.restore_path = exp_path
        test_ppl = lm_exp.test(poetry_sy_lm_dataset)  # fixme this is just a foo example, here there should be the actual testset
        print("PPL on Testset {}".format(test_ppl))
    else:
        print("\nEvaluation\n")
        lm_exp.config.restore_path = exp_path
        lm_exp.test(poetry_sy_lm_dataset)  # fixme this is just a foo example, here there should be the actual testset
