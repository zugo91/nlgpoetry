import os
import pickle
import re
from math import log2
import datetime as time

import tensorflow as tf
from tensorflow.contrib.tensorboard.plugins import projector

import numpy as np
from sklearn.model_selection import train_test_split

from Models.models import LanguageModel, Config
from Utils.utils import save_data, load_data, print_and_write
from Utils.utils import save_dictionary_tsv
from Utils.utils import build_dataset_of_tokens, create_lm_target, _batches
from Utils.utils import create_tercets, get_cantica
from Utils.utils import get_dc_cantos, build_stanzas_dataset_from_dict
from Utils.hyphenation import get_dc_hyphentation, print_paired_output

os.environ['CUDA_VISIBLE_DEVICES'] = '2'


def generate_text(start_string, sess, model, dictionary, rev_dictionary, use_f_cantica, logs):
    # Outputs the text generated when the model is free
    print_and_write(logs, "GENERATED TEXT from: " + ''.join(start_string))
    text_generated = []
    input_eval = [[dictionary[start] for start in start_string]]
    for i in range(config.sentence_max_len):
        gen_feed_dict = {model.x: input_eval, model.f_cantica: [[1,0,0]]} if use_f_cantica else {model.x: input_eval}
        o = sess.run(model.preds, feed_dict=gen_feed_dict)
        o = o[-1, -1]
        input_eval[-1].append(o)
        text_generated.append(rev_dictionary[o])
        text_generated = [t if t != '<SEP>' else ' ' for t in text_generated]
    print_and_write(logs, ''.join(start_string) + ''.join(text_generated))


def save_results(file, params, config=None, res=None):
    param_names = []
    for k, v in params.items():
        param_names.append(k)

    with open(file, 'a') as f:
        if not config:
            for p in param_names:
                f.write(p + '\t')
            f.write("PPL")
            f.write('\n')
        else:
            for p in param_names:
                f.write(str(getattr(config, p))+'\t')
            f.write(str(res))
            f.write('\n')


def get_hyp_lm_tercets(tercets):
    new_tercets = []
    for tercet in tercets:
        new_tercets.append([])
        for verse in tercet:
            new_tercets[-1].append([])
            for hyp_w in verse:
                new_tercets[-1][-1].extend(hyp_w)
                new_tercets[-1][-1].append('<SEP>')
            new_tercets[-1][-1] = new_tercets[-1][-1][:-1]

    return new_tercets


def get_hyp_lm_verses(tercets):
    verses = []
    for tercet in tercets:
        for verse in tercet:
            verses.append([[]])
            for hyp_w in verse:
                verses[-1][-1].extend(hyp_w)
                verses[-1][-1].append('<SEP>')
            verses[-1][-1] = verses[-1][-1][:-1]

    return verses



def get_label_weights(labels_df, special_tokens):
    class_weight = [0.1 for _ in range(len(labels_df) + len(special_tokens))]
    for t, token in enumerate(labels_df):
        # class_weight[t] = (1/(token[1]+1))*log(n_examples)
        class_weight[t] = 1/log2(token[1]+2)

    return class_weight


def run_exp(model_path, config):
    filename = os.path.join(os.getcwd(), "datasets", "la_divina_commedia.txt")
    canti, _, raw = get_dc_cantos(filename=filename, encoding='latin-1')
    cantica = get_cantica(filename=filename, encoding='latin-1')
    canti, tokens = get_dc_hyphentation(canti)

    tercets, f_cantica = create_tercets(list(zip(canti, cantica)))

    # tercets = get_hyp_lm_verses(tercets)
    tercets = get_hyp_lm_tercets(tercets)

    if not os.path.exists(model_path):
        os.mkdir(model_path)

    logs = os.path.join(model_path, "logs.txt")
    logs = open(logs, "w")

    save_path = os.path.join(model_path, "model.ckpt")
    restore_path = os.path.join(os.getcwd(), "savings", "It_exp", 'it_pre_train_lm_1', 'pretrained_lm', 'model.ckpt')
    dict_file = os.path.join(model_path, 'hyp_dictionary.pkl')
    rev_dict_file = os.path.join(model_path, 'hyp_rev_dictionary.pkl')
    count_dict_file = os.path.join(model_path, 'hyp_count_dictionary.pkl')

    if os.path.isfile(dict_file) and os.path.isfile(rev_dict_file):
        dictionary = load_data(dict_file)
        rev_dictionary = load_data(rev_dict_file)
        count = load_data(count_dict_file)
    else:
        data, count, dictionary, rev_dictionary = build_dataset_of_tokens(tokens,
                                                                          config.output_vocab_size - len(config.special_tokens),
                                                                          config.special_tokens)

        save_data(dictionary, dict_file)
        save_data(rev_dictionary, rev_dict_file)
        save_data(count, count_dict_file)

    metadata_filepath = os.path.join(model_path, 'metadata.tsv')
    save_dictionary_tsv(metadata_filepath, dictionary, count)

    # splits train/val split
    train_tercets, val_tercets, train_f_cantica, val_f_cantica = train_test_split(tercets, f_cantica, test_size=0.2, random_state=42)

    # creates training data
    train_dataset = build_stanzas_dataset_from_dict(train_tercets, dictionary, config=config, shuffle=False)
    # train_dataset = [v[:-1] for v in train_dataset]
    x, y = train_dataset, create_lm_target(train_dataset, config=config)

    # creates validation data
    val_dataset = build_stanzas_dataset_from_dict(val_tercets, dictionary, config=config, shuffle=False)
    # val_dataset = [v[:-1] for v in val_dataset]
    val_x, val_y = val_dataset, create_lm_target(val_dataset, config=config)

    print("Training Language Model")
    train_size = len(train_tercets)
    print_and_write(logs, "Train size: " + str(train_size))

    # labels_weight = get_label_weights(count, config.get_special_tokens_ids())
    x_ph = tf.placeholder(dtype=tf.int64, shape=[None, None])  # batch_size x sentence_max_len
    y_ph = tf.placeholder(dtype=tf.int64, shape=[None, None])  # batch_size x sentence_max_len

    with tf.variable_scope(author):
        name = "HyphenLanguageModel"
        hyp_lm = LanguageModel(x_ph, y_ph, config, global_scope=name)
        config.is_test = True
        config.encoder_keep_prob = 1.0
        hyp_lm_val = LanguageModel(x_ph, y_ph, config, reuse=True, global_scope=name)
        config.encoder_keep_prob = 0.7
        config.is_test = False

    # Tensorboard Summaries
    summary_writer_path = os.path.join(model_path, 'summary')
    summary_writer = tf.summary.FileWriter(summary_writer_path)
    with tf.name_scope("Summaries"):
        train_summary = tf.summary.scalar(name="Training PPL", tensor=hyp_lm.ppl)
        val_summary = tf.summary.scalar(name="Validation PPL", tensor=hyp_lm_val.ppl)
        # wo_val_summary = tf.summary.scalar(name="Words Only Validation PPL", tensor=hyp_lm_val.words_only_ppl)
        summary_proj_config = projector.ProjectorConfig()
        embedding = summary_proj_config.embeddings.add()
        embedding.tensor_name = hyp_lm.encoder.embeddings.name
        # Specify where you find the metadata
        embedding.metadata_path = metadata_filepath  # 'metadata.tsv'
        projector.visualize_embeddings(summary_writer, summary_proj_config)

    config_proto = tf.ConfigProto()
    config_proto.gpu_options.allow_growth = True
    saver = tf.train.Saver()
    with tf.Session(config=config_proto) as sess:
        if config.restore_model:
            print_and_write(logs, "Restoring Model in " + str(restore_path))
            sess.run(tf.global_variables_initializer())
            restorer = tf.train.Saver(var_list=[v for v in tf.trainable_variables() if "Cantica" not in v.name])
            # restorer = tf.train.Saver()
            restorer.restore(sess, restore_path)
        else:
            sess.run(tf.global_variables_initializer())

        tf.summary.FileWriter(summary_writer_path, sess.graph)

        use_cantica = config.use_f_cantica
        # val_x, val_y = val_x[:100], val_y[:100] #FIXME rimuovere
        val_feed_dict = {x_ph: val_x, hyp_lm_val.f_cantica: val_f_cantica, y_ph: val_y} if use_cantica else {x_ph: val_x,
                                                                                                             y_ph: val_y}
        train_sample_feed_dict = {x_ph: x[:10], hyp_lm_val.f_cantica: train_f_cantica[:10], y_ph: y[:10]} if use_cantica else {x_ph: x[:10],
                                                                                                                          y_ph: y[:10]}

        step, best_ppl = 0, 1e12
        for e in range(config.n_epochs):
            print_and_write(logs, "Epoch: " + str(e))
            trainset = (x,  train_f_cantica, y) if use_cantica else (x, y)
            for batch in _batches(trainset, config.batch_size):
                if use_cantica:
                    batch_x, batch_fc, batch_y = zip(*batch)
                    train_feed_dict = {x_ph: batch_x, hyp_lm.f_cantica: batch_fc, y_ph: batch_y}
                else:
                    batch_x, batch_y = zip(*batch)
                    train_feed_dict = {x_ph: batch_x, y_ph: batch_y}

                _, train_ppl_summary, loss, ppl, dec_p, dec_o = sess.run((hyp_lm.train_op,
                                                                          train_summary,
                                                                          hyp_lm.loss,
                                                                          hyp_lm.ppl,
                                                                          hyp_lm.preds,
                                                                          hyp_lm.logits),
                                                                         feed_dict=train_feed_dict)

                if step%100 == 0:
                    summary_writer.add_summary(train_ppl_summary, step)
                    print_and_write(logs, "Step: " + str(step) + "\n")
                    print_and_write(logs, "Train mini-batch Loss: " + str(loss) + "\n")
                    print_and_write(logs, "Train mini-batch Perplexity: " + str(ppl) + "\n")

                    preds = sess.run((hyp_lm_val.preds), feed_dict=train_sample_feed_dict)
                    print_and_write(logs, "Training Fitting: \n")
                    print_paired_output(logs, y[:10], preds[:10], rev_dictionary,
                                        special_tokens=[config._PAD, 0, config._SEP, config._GO])

                    val_ppl_summary, preds, loss, ppl = sess.run((val_summary,
                                                                  hyp_lm_val.preds,
                                                                  hyp_lm_val.loss,
                                                                  hyp_lm_val.ppl),
                                                                 feed_dict=val_feed_dict)

                    summary_writer.add_summary(val_ppl_summary, step)

                    print_and_write(logs, "Validation: \n")
                    print_paired_output(logs, val_y[:10], preds[:10], rev_dictionary, special_tokens=[config._PAD, 0, config._SEP, config._GO])
                    print_and_write(logs, "Validation Loss: " + str(loss) + "\n")
                    print_and_write(logs, "Validation Perplexity: " + str(ppl) + "\n")

                    # Model Checkpoint, only if improved
                    if ppl < best_ppl:
                        print_and_write(logs, "Saving Best Model at step " + str(step) + "\n")
                        saver.save(sess, save_path)

                    # update best score ppl
                    best_ppl = ppl if best_ppl > ppl else best_ppl

                if step % 500 == 0:
                    start_string = ['<GO>', 'a', 'mor']
                    generate_text(start_string, sess, hyp_lm_val, dictionary, rev_dictionary, use_cantica, logs)

                # if step > config.n_steps:
                #     break
                step += 1

        preds, loss = sess.run((hyp_lm_val.preds, hyp_lm_val.loss), feed_dict=val_feed_dict)
        print_and_write(logs, "Final Validation Loss: " + str(loss) + "\n")
        print_and_write(logs, "Final Validation Perplexity: " + str(np.exp(loss)) + "\n")
        print_and_write(logs, "Best Validation Perplexity: " + str(best_ppl) + "\n")
        logs.close()

        return best_ppl


if __name__ == '__main__':
    author = "It_exp"
    is_test = False
    print(author)

    config = Config(vocab_size=1884)
    config.sentence_max_len = 75
    config.batch_size = 16
    config.n_epochs = 15
    config.restore_model = True

    dict = {
        "input_emb_size": [300,],
        "encoder_rnn_size": [1024],
        "encoder_keep_prob": [0.7],
        # "proj_size": [512, 1024],
        "lr": [0.001, 0.003],
        "restore_model": [True, False],
        "use_f_cantica": [True, False],
        "batch_size": [16, 32]
    }

    exp_dir = "dante_lm_exp"
    if not is_test:
        os.mkdir(os.path.join(os.getcwd(), "savings", author, exp_dir))
        res_file = os.path.join(os.getcwd(), "savings", author, exp_dir, 'results.csv')

        save_results(res_file, dict)
        for config in config.set_params(dict):
            config.set_tied_params()
            model_dir = "hyp_lm_exp_we" + str(config.input_emb_size) + '_e' + \
                        str(config.encoder_rnn_size) + '_dr' + \
                        str(config.encoder_keep_prob) + '_lr' + \
                        str(config.lr) + '_bs' + \
                        str(config.batch_size) + '_rs' + \
                        str(config.restore_model) + '_fc' + \
                        str(config.use_f_cantica) + '_'  # + \
                        # str(time.datetime.now())
            model_path = os.path.join(os.getcwd(), "savings", author, exp_dir, model_dir)
            tf.reset_default_graph()
            best_ppl = run_exp(model_path, config)
            save_results(res_file, dict, config, best_ppl)