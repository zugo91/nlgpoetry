import tensorflow as tf


def cell(size, cell_type, dropout=None, layers=1, config=None):
    cells = []
    cell = None
    for _ in range(layers):
        if cell_type == "LSTM":
            cell = tf.contrib.rnn.BasicLSTMCell(size)
        elif cell_type == "GRU":
            cell = tf.contrib.rnn.GRUCell(size)
        elif cell_type == "FUN":
            cell = FunRNNCell(size,
                              k=config.fun_centers,
                              lambda_alphas=config.fun_lambda_alphas,
                              kernel=config.fun_activation,
                              batch_norm=config.batch_norm)
        else:
            cell = tf.contrib.rnn.BasicRNNCell(size)

        if dropout:
            cell = tf.nn.rnn_cell.DropoutWrapper(cell, dtype=tf.float32, input_keep_prob=dropout, output_keep_prob=1.0,
                                                 state_keep_prob=1.0)
        cells.append(cell)
    if layers == 1:
        return cell
    else:
        return tf.contrib.rnn.MultiRNNCell(cells)


class SimpleNlpRnn(object):
    def __init__(self, config):
        self.config = config

        with tf.name_scope("Embeddings"):
            self.embeddings = tf.get_variable("InputEmbeddings", [self.config.input_vocab_size, self.config.input_emb_size],
                                              dtype=tf.float32)

        with tf.name_scope("RNN"):
            with tf.variable_scope("rnn"):
                self.sequences_rnn_cell = cell(self.config.encoder_rnn_size, self.config.cell_type,
                                               dropout=self.config.encoder_keep_prob,
                                               config=config)

    def __call__(self, inputs, initial_state=None):
        self.sequences = inputs
        self.wes = tf.nn.embedding_lookup(self.embeddings,
                                          self.sequences)  # batch_size x sentence_max_len x word_emb_size

        outputs, state = tf.nn.dynamic_rnn(cell=self.sequences_rnn_cell,
                                           inputs=self.wes,
                                           initial_state=initial_state,
                                           dtype=tf.float32)
        # batch_size x encoder_rnn_size
        # print(state)
        if self.config.cell_type == "LSTM":
            state = state.h
        # elif self.config.cell_type == "LSTM" and self.config.wrap_attention:
        #     state = state[0]

        return state, outputs


class SimpleRnn(object):
    def __init__(self, seq, config, initial_state=None):
        self.sequences = seq
        print(self.sequences)

        with tf.name_scope("RNN"):
            with tf.variable_scope("rnn"):
                self.sequences_rnn_cell = cell(config.encoder_rnn_size, "LSTM",
                                               dropout=config.encoder_keep_prob,
                                               config=config)

            outputs, state = tf.nn.dynamic_rnn(cell=self.sequences_rnn_cell,
                                               inputs=self.sequences,
                                               initial_state=initial_state,
                                               dtype=tf.float32)

            # batch_size x encoder_rnn_size
            print(state)
            self.state_h = state[0]  # state.h
            self.state_c = state[1]  # state.c
            print(self.state_h)

            self.rnn_outputs = outputs


class CharEncoder(object):
    def __init__(self, sentences, config):
        self.sentences = sentences
        batch_size = tf.shape(self.sentences)[0]
        sentence_len = tf.shape(self.sentences)[1]
        with tf.name_scope("Embeddings"):
            self.embedding = tf.get_variable("CharEmbeddings", [config.char_vocab_size, config.char_embed_size],
                                        dtype=tf.float32)
            chars = tf.nn.embedding_lookup(self.embedding,
                                           self.sentences)  # batch_size x sentence_max_len x word_max_len x char_embed_size
            chars = tf.reshape(chars,
                               [batch_size * sentence_len, config.word_max_len,
                                config.char_embed_size])

        with tf.name_scope("Encoding"):
            with tf.name_scope("MorphologyEncoding"):
                with tf.variable_scope("MorphologicEncoder"):
                    with tf.variable_scope("fw"):
                        char_rnn_cell_fw = cell(config.morph_rnn_size, "LSTM", dropout=config.morph_encoder_keep_prob)
                    with tf.variable_scope("bw"):
                        char_rnn_cell_bw = cell(config.morph_rnn_size, "LSTM", dropout=config.morph_encoder_keep_prob)

                    _, (fw_state, bw_state) = tf.nn.bidirectional_dynamic_rnn(cell_fw=char_rnn_cell_fw,
                                                                              cell_bw=char_rnn_cell_bw,
                                                                              inputs=chars,
                                                                              dtype=tf.float32)

                output = tf.concat((fw_state.h, bw_state.h), axis=1)
                # batch_size x sentence_max_len x 2 * config.morph_rnn_size
                self.char_encodings = tf.reshape(output,
                                                 [batch_size, sentence_len,
                                                  2 * config.morph_rnn_size])


class Encoder(object):
    def __init__(self, config, reuse=False):
        self.config = config

        with tf.variable_scope("Embeddings", reuse=reuse):
            self.embeddings = tf.get_variable("InputEmbeddings", [self.config.input_vocab_size, self.config.input_emb_size],
                                              dtype=tf.float32)

        with tf.variable_scope("Encoding", reuse=reuse):
            with tf.variable_scope("fw"):
                self.sentences_rnn_cell_fw = cell(self.config.encoder_rnn_size, self.config.cell_type,
                                                  dropout=self.config.encoder_keep_prob)
            with tf.variable_scope("bw"):
                self.sentences_rnn_cell_bw = cell(self.config.encoder_rnn_size, self.config.cell_type,
                                                  dropout=self.config.encoder_keep_prob)

    def __call__(self, inputs):
        self.wes = tf.nn.embedding_lookup(self.embeddings,
                                          inputs)  # batch_size x sentence_max_len x word_emb_size

        outputs, (fw_state, bw_state) = tf.nn.bidirectional_dynamic_rnn(cell_fw=self.sentences_rnn_cell_fw,
                                                                        cell_bw=self.sentences_rnn_cell_bw,
                                                                        inputs=self.wes,
                                                                        dtype=tf.float32)

        # batch_size x 2*encoder_rnn_size
        if self.config.cell_type == "LSTM":
            encodings = tf.concat((fw_state.h, bw_state.h), axis=1)
        else:
            encodings = tf.concat((fw_state, bw_state), axis=1)

        self.left_context = outputs[0]
        self.right_context = outputs[1]
        outputs = tf.concat((outputs[0], outputs[1]), axis=2)

        return encodings, outputs


class Decoder(object):
    def __init__(self, encodings, config, global_scope='Decoder', reuse=False):
        with tf.variable_scope("Decoder", reuse=reuse):
            self.final_state, self.states = encodings[0], encodings[1]
            with tf.name_scope("Embeddings"):
                self.dec_embeddings = tf.get_variable("OutputEmbeddings", [config.output_vocab_size, config.output_emb_size], dtype=tf.float32)
                # self.dec_wes = tf.nn.embedding_lookup(self.dec_embeddings,
                #                                   self.sentences)  # batch_size x sentence_max_len x word_emb_size
            with tf.name_scope("Decoding"):
                batch_size = tf.shape(self.final_state)[0]
                # decoded_words = self.final_state
                # decoded_words = tf.reshape(encodings, [-1, config.character_decoder_size])
                decoding_cell = cell(size=config.decoder_rnn_size,
                                     dropout=config.decoder_keep_prob,
                                     cell_type="GRU")

                if config.wrap_attention:
                    print(self.states)
                    print(tf.shape(self.states))
                    # attention_mechanism = tf.contrib.seq2seq.LuongAttention(
                    #     config.decoder_rnn_size, self.states,
                    #     memory_sequence_length=75)
                    attention_mechanism = tf.contrib.seq2seq.BahdanauAttention(
                        num_units=config.proj_size, memory=self.states)
                    decoding_cell = tf.contrib.seq2seq.AttentionWrapper(decoding_cell, attention_mechanism)
                    decoding_cell = tf.contrib.rnn.OutputProjectionWrapper(
                            decoding_cell, config.output_vocab_size, reuse=reuse)

                helper = tf.contrib.seq2seq.GreedyEmbeddingHelper(
                    embedding=self.dec_embeddings,
                    start_tokens=tf.tile([config._GO], [batch_size]),
                    end_token=config._EOT)

                # initial_decoder_state = tf.contrib.rnn.LSTMStateTuple(c=tf.zeros_like(decoded_words),
                #                                                       h=decoded_words)
                # initial_decoder_state = self.final_state
                initial_decoder_state = decoding_cell.zero_state(dtype=tf.float32, batch_size=batch_size)

                print(initial_decoder_state)
                decoder = tf.contrib.seq2seq.BasicDecoder(
                    cell=decoding_cell,
                    helper=helper,
                    initial_state=initial_decoder_state)
                final_outputs, _, _ = tf.contrib.seq2seq.dynamic_decode(
                    decoder=decoder,
                    output_time_major=False,
                    impute_finished=True,
                    maximum_iterations=config.sentence_max_len)

                missing_time_steps = config.sentence_max_len - tf.shape(final_outputs.rnn_output)[1]
                final_outputs = tf.pad(final_outputs.rnn_output, [[0, 0], [0, missing_time_steps], [0, 0]])
                # tf.Print(final_outputs, [final_outputs])
                self.decoded_outputs = tf.reshape(final_outputs, [-1, config.sentence_max_len, config.output_vocab_size])
                self.decoded_preds = tf.argmax(self.decoded_outputs, axis=2)


class AttentionRNNDecoder(object):
    def __init__(self, config, reuse=False):
        self.config = config
        with tf.name_scope("Embeddings"):
            self.embeddings = tf.get_variable("DecoderEmbeddings", [self.config.input_vocab_size, self.config.input_emb_size],
                                              dtype=tf.float32)

        with tf.name_scope("RNN"):
            with tf.variable_scope("Fx"):
                self.sequences_rnn_cell = cell(self.config.rnn_size, self.config.cell_type,
                                               dropout=self.config.keep_prob,
                                               config=self.config)
            with tf.variable_scope("Gx"):
                self.proj = tf.layers.Dense(self.config.proj_size)

            with tf.variable_scope("AttentionScore"):
                self.aW = tf.get_variable("AttentionScoreMatrix", [self.config.rnn_size, self.config.memory_size],
                                              dtype=tf.float32)

            with tf.variable_scope("AttentionProj"):
                self.att_proj = tf.layers.Dense(self.config.rnn_size, activation=tf.nn.tanh)


    def __call__(self, inputs, attention_memory):
        '''
        :param inputs: A 2-d tensor of inputs, e.g. [[<GO>]].
        :param attention_memory: a 3-d tensor of dimensions like [batch_size, memory_len, memory_size].
        It typically contains encoder states.
        :return: outputs a 2-d tensor with decoder predictions
        '''

        self.sequences = inputs
        self.h_s = attention_memory
        self.batch_size = tf.shape(self.sequences)[0]

        # Initial state of the Cell.
        initial_state = h_t = self.sequences_rnn_cell.zero_state(self.batch_size, dtype=tf.float32)

        all_logits = []
        outputs = []  # TODO verify correctness
        for t in range(self.config.sentence_max_len):
            # The value of state is updated after processing each batch of words.
            self.wes = tf.nn.embedding_lookup(self.embeddings,
                                              self.sequences)  # batch_size x 1 x word_emb_size
            self.wes = tf.reshape(self.wes, [-1, self.config.input_emb_size]) # batch_size x word_emb_size
            output, h_t = self.sequences_rnn_cell(self.wes, h_t)  # batch_size x rnn_size

            if self.config.wrap_attention:
                score = self.score(self.h_s, h_t)
                alphas = tf.nn.softmax(score)  # attention weights: batch_size x memory_len
                alphas = tf.reshape(alphas, [self.batch_size, self.config.memory_len, 1])
                c_t = tf.reduce_sum(tf.multiply(alphas, self.h_s), axis=1)  # batch_size x memory_size
                a_t = tf.concat((c_t, h_t), axis=-1)  # batch_size x (memory_size + rnn_size)

                h_t = self.att_proj(a_t)  # next input state

            proj = self.proj(h_t)
            self.logits_t = tf.matmul(proj, tf.transpose(self.embeddings))  # batch_size x vocab_size
            all_logits.append(tf.expand_dims(self.logits_t, axis=1))
            self.sequences = tf.argmax(self.logits_t, axis=1)  # next input token
            outputs.append(tf.expand_dims(self.sequences, axis=-1))

        final_state = h_t
        self.logits = tf.concat(all_logits, axis=1)
        self.outputs = tf.concat(outputs, axis=-1)

        return self.logits, self.outputs

    def score(self, hs, ht):
        # FIXME simple similarity score, not good enough
        ht = tf.reshape(ht, shape=[self.batch_size, 1, self.config.rnn_size])
        return tf.reduce_sum(tf.multiply(hs, ht), axis=-1)  # batch_size x memory_len



eps = 1e-12


def fun_activation(x, k=10, kernel=tf.abs, lambda_alphas=0.):

    batch_size, size = tf.shape(x)[0], x.get_shape()[1].value

    # get (or create) center variables for kernel functions
    centers = tf.get_variable("fun_centers", shape=[k,size], initializer=tf.truncated_normal_initializer)
    # compute kernel on input
    x = tf.tile(tf.expand_dims(x,1), [1, k, 1])
    mu  = tf.tile(tf.expand_dims(centers, 0), [batch_size,1,1])
    diff = x- mu
    G = kernel(diff)

    # get (or create) alphas and weight kernel functions
    alphas = tf.get_variable("fun_alphas", shape=[k, size], initializer=tf.zeros_initializer)

    a_G = tf.multiply(alphas, G)

    '''Regularization terms'''

    # minimize number of activated alphas
    # tf.add_to_collection("losses",
    #                      lambda_alphas * tf.reduce_sum(tf.abs(alphas)))

    return tf.reduce_sum(a_G, axis=1)


class FunRNNCell(tf.contrib.rnn.RNNCell):

    def __init__(self, num_units, k, lambda_alphas=0., kernel=tf.abs, batch_norm=False, reuse=None):
        super(FunRNNCell, self).__init__(_reuse=reuse)
        self._num_units = num_units
        self.k = k
        self.kernel = kernel
        self.lambda_alphas = lambda_alphas
        self.batch_norm = batch_norm


    @property
    def state_size(self):
        return self._num_units

    @property
    def output_size(self):
        return self._num_units

    def call(self, inputs, state):
        new_input = tf.concat([inputs, state], 1)

        new_input_size = new_input.get_shape()[1].value

        with tf.variable_scope("FUNCell"):
            w1 = tf.get_variable("w1", shape=[new_input_size, self._num_units],
                                 initializer=tf.truncated_normal_initializer)
            b1 = tf.get_variable("b1", shape=[self._num_units], initializer=tf.zeros_initializer)

            a1 = tf.matmul(new_input, w1) + b1

            if self.batch_norm:
                a1 = tf.contrib.layers.batch_norm(a1, center=True, scale=True, scope='bn')

            with tf.variable_scope("state"):
                state = fun_activation(a1, k=self.k, lambda_alphas=self.lambda_alphas, kernel=self.kernel)

        return state, state