# Neural Poetry: Learning to Generate Poems Using Syllables


## Install
```
git clone git@gitlab.com:zugo91/nlgpoetry.git
```

## Citing
```
@inproceedings{zugarini2019neural,
  title={Neural Poetry: Learning to Generate Poems Using Syllables},
  author={Zugarini, Andrea and Melacci, Stefano and Maggini, Marco},
  booktitle={International Conference on Artificial Neural Networks},
  pages={313--325},
  year={2019},
  organization={Springer}
}
```
