import os
import tensorflow as tf
import numpy as np

from nlp.utils.basic_utils import print_and_write

TEST_SIZE = 250


class Experiment(object):
    def __init__(self, model, model_val, model_gen, dataset, exp_path, config):
        self.model = model
        self.model_val = model_val
        self.model_gen = model_gen
        self.dataset = dataset
        self.exp_path = exp_path
        self.save_path = os.path.join(self.exp_path, "model.ckpt")

        self.summary_writer_path = os.path.join(self.exp_path, 'summary')
        self.summary_writer = tf.summary.FileWriter(self.summary_writer_path)

        self.config = config

    @staticmethod
    def get_config_proto():
        config_proto = tf.ConfigProto()
        config_proto.gpu_options.allow_growth = True
        return config_proto

    @staticmethod
    def save_model(sess, logs, global_step, saver, save_path):
        """
        Model Checkpoint
        :param sess: tf.Session
        :param logs: log file
        :param global_step: integer
        :param saver: tf.Saver() object
        :param save_path: file path where to save the checkpoint
        :return:
        """
        print_and_write(logs, "\nSaving Best Model at step " + str(global_step) + "\n")
        saver.save(sess, save_path)

    @staticmethod
    def restore_model(logs, sess, restore_path, var_list=None):
        # Restore variables from pretrained model (restore path)
        print_and_write(logs, "Restoring Model in " + str(restore_path))
        sess.run(tf.global_variables_initializer())
        var_list = tf.global_variables() if var_list is None else var_list
        restorer = tf.train.Saver(var_list=var_list)
        restorer.restore(sess, restore_path)
        # sess.run(tf.variables_initializer(op))

    @staticmethod
    def early_stop(logs, stop_cond, max_no_improve):
        if stop_cond >= max_no_improve:
            print_and_write(logs, "\nEARLY STOPPING\n")
            return True

        return False

    @staticmethod
    def checkpoint_manager(sess, logs, loss, best_loss, best_step, stop_cond, restore_cond, global_step, saver, save_path, max_restore_cond=10):
        """
        Handles savings of best models and restoring of previous ones
        when performances do not improve anymore
        :param sess: tf.Session() object
        :param logs: an opened file where to save logs
        :param loss: current loss value
        :param best_loss: best loss value
        :param best_step: step of the best_loss
        :param stop_cond: Number of steps, without improvements
        :param restore_cond: Number of steps without improvements since the last restore
        :param global_step: current step
        :param saver: tf.train.Saver() object
        :param save_path: path where to save the model
        :param max_restore_cond: number of steps before restore
        :return:
        """
        # Model Checkpoint, only if improved
        if loss < best_loss:
            Experiment.save_model(sess, logs, global_step, saver, save_path)
            stop_cond, restore_cond = 0, 0
            best_step = global_step
        else:
            stop_cond += 1
            restore_cond += 1
            if restore_cond%max_restore_cond == 0:
                Experiment.restore_model(logs, sess, save_path)

        # update best score ppl
        best_loss = loss if best_loss > loss else best_loss
        return best_loss, stop_cond, restore_cond, best_step

    @staticmethod
    def add_visualization_summaries(name, tensor):
        with tf.name_scope("Summaries"):
            return tf.summary.scalar(name=name, tensor=tensor)

    def get_initial_exp_conds(self):
        return 1e12, 0, 0, self.config.learning.max_no_improve

    def lr_scheduler(self, logs, value, restore_cond, max_restore_cond=10,  min_lr=3e-5):
        if self.config.learning.lr_scheduler and restore_cond > 0 and restore_cond % max_restore_cond == 0:
            print_and_write(logs, "Setting learning rate to %f " % value)
            self.model.set_lr(max(value, min_lr))

    def run(self):
        return NotImplementedError

    def _updt_train_logs(self, logs, batch_outputs, step):
        for summary in batch_outputs["summaries"]:
            self.summary_writer.add_summary(summary, step)

    def _updt_val_logs(self, logs, val_outputs, step):
        summaries = [o["ppl"] for o in val_outputs]
        val_ppl = np.mean(np.array(summaries))
        summary = tf.Summary()
        summary.value.add(tag="val ppl", simple_value=val_ppl)
        self.summary_writer.add_summary(summary, step)

    def render(self, logs, examples, preds, gen_size):
        return NotImplementedError

    def _get_val_batch(self, val_range):
        """

        :param val_range: tuple of two elements (start, end)
        :return: a batch of validation examples
        """
        return NotImplementedError

    @staticmethod
    def _get_test_batch(dataset, test_size):
        return NotImplementedError

    def _get_gen_batch(self, gen_size):
        return NotImplementedError

    def _generation_op(self, sess, gen_batch):
        return self.model_gen.gen_op(sess, gen_batch)

    def _validation_op(self, sess, logs, val_summaries, step, val_size, gen_size, best_loss, best_step, stop_cond, restore_cond, saver):
        """
        Runs the model on the validation set
        :param sess: tf.Session() object
        :param logs: an opened file where to save logs
        :param val_summaries: a list of summaries for validation
        :param step: current time step
        :param val_size: number of examples from the validation to assess
        :param gen_size: number of examples to show, rendering them
        :param best_loss: current best loss value
        :param best_step: current best step
        :param stop_cond: Number of steps, without improvements
        :param restore_cond: Number of steps without improvements since the last restore
        :param saver: tf.train.Saver() object
        :return: Updated values of best_loss, stop_cond, restore_cond, best_step
        """

        print_and_write(logs, "STEP:" + str(step))
        print_and_write(logs, "\n\n VALIDATION EVALUATION \n\n")

        # evaluates the model on the whole validation set. The evaluation is averaged among all
        # the batches of validation
        start = 0
        val_outputs = []
        val_batch = []
        for j in range(len(self.dataset.val_x) // val_size):
            val_batch = self._get_val_batch((start, start + val_size))
            val_outputs.append(self.model_val.val_op(sess, val_batch, val_summaries))
            start += val_size

        self._updt_val_logs(logs, val_outputs, step)

        gen_batch = self._get_gen_batch(gen_size)
        gen_outputs = self._generation_op(sess, gen_batch)

        self.render(logs, [val_batch[0][0]], gen_outputs["preds"].tolist(), gen_size)  # render the last batch

        val_loss = np.mean(np.array([o["to_optimize_loss"] for o in val_outputs]))
        best_loss, stop_cond, restore_cond, best_step = self.checkpoint_manager(sess, logs, val_loss, best_loss,
                                                                                best_step, stop_cond, restore_cond, global_step=step,
                                                                                saver=saver, save_path=self.save_path)

        if self.config.learning.lr_scheduler:
            # decrease lr by 1.5
            self.lr_scheduler(logs, self.model.lr / 1.5, restore_cond)

        return best_loss, stop_cond, restore_cond, best_step

    def _run_train_session(self, logs, train_summaries, val_summaries, val_size, gen_size):
        saver = tf.train.Saver()
        with tf.Session(config=self.get_config_proto()) as sess:
            sess.run(tf.compat.v1.global_variables_initializer())
            if self.config.restore_model:
                restore_path = os.path.join(self.config.restore_path, "model.ckpt")
                self.restore_model(logs, sess, restore_path)

            tf.summary.FileWriter(self.summary_writer_path, sess.graph)

            self.dataset.initialize(sess)

            step = 0
            best_step = -1
            best_loss, stop_cond, restore_cond, max_no_improve = self.get_initial_exp_conds()  # early stops conditions
            batch_size = self.config.learning.batch_size

            for e in range(self.config.learning.n_epochs):
                print_and_write(logs, "Epoch: " + str(e))
                for batch in self.dataset.get_batches(batch_size):

                    batch_output = self.model.run_train_op(sess, batch, train_summaries)  # train step

                    if step % 100 == 0:
                        self._updt_train_logs(logs, batch_output, step)

                    step += 1

                # after each epoch, validate on the validation set
                best_loss, stop_cond, restore_cond, best_step = self._validation_op(sess, logs, val_summaries, step, val_size, gen_size,
                                                                                    best_loss, best_step, stop_cond, restore_cond,
                                                                                    saver)

                if self.early_stop(logs, stop_cond, max_no_improve):
                    break

        return best_loss, best_step

    def run_gen_session(self, gen_size=1):
        """
        RUN generation
        :param gen_size:
        :return:
        """

        logs = os.path.join(self.exp_path, "gen_logs.txt")
        logs = open(logs, "w")

        # Session Configs and Saver
        config_proto = tf.ConfigProto()
        config_proto.gpu_options.allow_growth = True
        with tf.Session(config=config_proto) as sess:
            # Model initialization
            # Restore variables from pretrained model (restore path)
            sess.run(tf.global_variables_initializer())
            restore_path = os.path.join(self.config.restore_path, "model.ckpt")
            self.restore_model(logs, sess, restore_path, var_list=tf.trainable_variables())

            gen_batch = self._get_gen_batch(gen_size)
            gen_outputs = self.model_val.gen_op(sess, gen_batch)
            self.render(logs, gen_batch, gen_outputs["preds"], gen_size)

    def test(self, dataset):
        """
        Evaluates the model on an input dataset.

        WARNING: It is assumed all the data to evaluate is in dataset.val_* attributes.
        This clearly indicates that train/val pair in dataset is not generic enough
        and therefore the dataset structure and data handling needs a significant update.
        Data handling will be updated in a future version TODO.

        :param dataset: an object of class Dataset or class inheriting from Dataset.
        :return: ppl on the given dataset.
        """

        logs = os.path.join(self.exp_path, "test_logs.txt")
        logs = open(logs, "w")

        test_summary = self.add_visualization_summaries("Test PPL", self.model_val.ppl)
        test_summaries = [test_summary]

        test_batch_size = TEST_SIZE
        start = 0
        test_outputs = []
        with tf.Session(config=self.get_config_proto()) as sess:
            restore_path = os.path.join(self.config.restore_path, "model.ckpt")
            self.restore_model(logs, sess, restore_path)
            for j in range(len(dataset.val_x)//test_batch_size):
                test_batch = self._get_test_batch(dataset, (start, start+test_batch_size))
                test_outputs.append(self.model_val.val_op(sess, test_batch, test_summaries))
                start += test_batch_size

        summaries = [o["ppl"] for o in test_outputs]
        test_ppl = np.mean(np.array(summaries))

        return test_ppl
