
# coding: utf-8

# In[1]:

import os
import xml.etree.ElementTree as ET


# In[2]:

def remove_newline(txt):
    return str.replace(txt.rstrip('\n\r'), '\n', '')

def get_prose(elementTree):
    all_data = []
    for par in elementTree.findall('p'):
        all_data.append(remove_newline(''.join(par.itertext())))                    
                
    return all_data

def get_poetry(elementTree):
    all_data = []
    for composition in elementTree.findall('lg'):
        poetry = []
        for verse in composition.iter('l'):
            cleaned_verse = remove_newline(''.join(verse.itertext()))
            if len(cleaned_verse) > 1:
                poetry.append(cleaned_verse)
        all_data.append(poetry)
    
    return all_data


# In[3]:

def get_proses(filename):
    tree = ET.parse(filename)
    root = tree.getroot()
    all_data = []
    for div in root.iter('div1'):       
            all_data.extend(get_prose(div))
    
    return all_data

def get_poetries(filename):
    tree = ET.parse(filename)
    root = tree.getroot()
    poetries = []
    for div in root.iter('div1'):       
            poetries.extend(get_poetry(div))
    
    return poetries

ilfiore_filename = os.path.join(os.getcwd(), '..', 'datasets' , 'dante' ,'ilfiore.xml')
ilfiore = get_poetries(ilfiore_filename)

dettodamore_filename = os.path.join(os.getcwd(), '..', 'datasets' , 'dante' ,'dettodamore.xml')
dettodamore = get_poetries(dettodamore_filename)

def get_lerime(filename):
    tree = ET.parse(filename)
    root = tree.getroot()
    poetries = []
    for div in root.iter('div1'):
        poetry = []
        for o in div.findall('opener'):
            div.remove(o)
        
        poetry = ''.join(div.itertext()).split('\n')
        poetries.append([remove_newline(v) for v in poetry if v])

    return poetries

rime_filename = os.path.join(os.getcwd(), '..', 'datasets' , 'dante' ,'rime.xml')
rime = get_lerime(rime_filename)


# In[4]:

rime


# In[5]:

def get_convivio_prose(filename):
    tree = ET.parse(filename)
    root = tree.getroot()
    all_data = []
    for part in root.iter('div1'):
        for chapter in part.iter('div2'):
            all_data.extend(get_prose(chapter))                    
                
    return all_data

def get_convivio_poetry(filename):
    tree = ET.parse(filename)
    root = tree.getroot()
    all_data = []
    for part in root.iter('div1'):
        for chapter in part.iter('div2'):            
                all_data.extend(get_poetry(chapter))
    
    return all_data

convivio_filename = os.path.join(os.getcwd(), '..', 'datasets' , 'dante' ,'convivio_fixed.xml')
convivio_prose = get_convivio_prose(convivio_filename)
convivio_poetry = get_convivio_poetry(convivio_filename)


# In[6]:

convivio_prose


# In[7]:

convivio_prose


# In[8]:

vita_nuova_filename = os.path.join(os.getcwd(), '..', 'datasets' , 'dante' ,'vitanuova.xml')
vita_nuova_prose = get_proses(vita_nuova_filename)
vita_nuova_poetries = get_poetries(vita_nuova_filename)


# In[13]:

vita_nuova_prose


# In[11]:

len(vita_nuova_poetries)


# In[ ]:



